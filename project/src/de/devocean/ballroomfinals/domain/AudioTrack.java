package de.devocean.ballroomfinals.domain;

public class AudioTrack {

	public static final AudioTrack NULL_TRACK = new AudioTrack("","", "", 0, "");

	private String title;
	private String artist;
	private String album;
	private int duration = 0;
	private int targetDuration = 0;
	private int targetTempoPercent = Defaults.getTempoPercentage();
	private String filePath;


	private Genre genre = Genre.NULL_GENRE;

	public AudioTrack() {}

	public AudioTrack(String title, String artist, String album, int duration, String filePath) {
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.duration = duration;
		this.filePath = filePath;
	}

	public String getTitle() {
		return title;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}


    public int getTargetDuration() {
    	if (targetDuration == 0) {
    		return getDuration();
    	}
    	return targetDuration;
    }


    public void setTargetDuration(int targetDuration) {
    	this.targetDuration = targetDuration;
    }

	public int getTargetTempoPercent() {
		return targetTempoPercent;
	}

	public void setTargetTempoPercent(int targetTempo) {
		this.targetTempoPercent = targetTempo;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFilePath(String path) {
		this.filePath = path;
	}

	public String toString() {
		return "[" + genre.getName() + "] " + title;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public String getGenreLabel() {
		return genre.getName();
	}

	public boolean isBreak() {
	    return false;
    }

}
