package de.devocean.ballroomfinals.domain;

import android.net.Uri;
import android.provider.MediaStore.Audio.Genres;

public class Genre {

	public static final Genre NULL_GENRE = new Genre(-1, "");
	public static final Genre BREAK_GENRE = new Genre(-2, "Pause");
	private int id;
	private String name;
	private int measuresPerMinute;

	public Genre(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Genre(int id, String name, int measuresPerMinute) {
		this.id = id;
		this.name = name;
		this.measuresPerMinute = measuresPerMinute;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getMeasuresPerMinute() {
		return measuresPerMinute;
	}

	public String toString() {
		return getName();
	}

	public Uri getUri() {
        return Uri.parse(
            new StringBuilder()
            .append(Genres.EXTERNAL_CONTENT_URI)
            .append("/")
            .append(id)
            .append("/")
            .append(Genres.Members.CONTENT_DIRECTORY)
            .toString());
    }

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (! (o instanceof Genre)) {
			return false;
		}
		return ((Genre)o).name.equals(name);
	}

}
