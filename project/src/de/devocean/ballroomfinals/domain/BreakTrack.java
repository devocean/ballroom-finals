package de.devocean.ballroomfinals.domain;


public class BreakTrack extends AudioTrack {

	public BreakTrack(int duration) {
		super("Silence", "", "", duration, "");
		this.setGenre(Genre.BREAK_GENRE);
		this.setTargetDuration(duration);
	}

	@Override
	public boolean isBreak() {
	    return true;
	}
	
	@Override
	public void setTargetDuration(int targetDuration) {
		super.setTargetDuration(targetDuration);
		setDuration(targetDuration);
	}

}
