package de.devocean.ballroomfinals.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import de.devocean.ballroomfinals.MusicLibrary;

public class Playlist {

	private ArrayList<AudioTrack> tracks = new ArrayList<AudioTrack>();

	private boolean repeat = false;
	private boolean shuffle = false;

	private int currentSongIndex = 0;

	private MusicLibrary musicLibrary;

	public void add(AudioTrack newTrack) {
		tracks.add(newTrack);
	}

	public AudioTrack getAt(int index) {
		if (index < 0 || tracks.size() <= index ) {
			return AudioTrack.NULL_TRACK;
		}
		currentSongIndex = index;
		return tracks.get(currentSongIndex);
	}

	public AudioTrack first() {
		return getAt(0);
	}

	public AudioTrack last() {
		return getAt(tracks.size() - 1);
	}

	public void addAll(List<AudioTrack> trackList) {
		for (AudioTrack track : trackList) {
			tracks.add(track);
		}
	}

	public boolean toggleShuffle() {
		shuffle = !shuffle;
		return shuffle;
	}

	public boolean toggleRepeat() {
		repeat = !repeat;
		return repeat;
	}

	private boolean atLastTrack() {
		return currentSongIndex == tracks.size() -1;
	}
	
	public AudioTrack getCurrentTrack() {
		return getAt(currentSongIndex );
	}

	public AudioTrack getPreviousTrack() {
		currentSongIndex = currentSongIndex - 1;
		if (currentSongIndex < 0 && repeat) {
			currentSongIndex = tracks.size() - 1;
		}
		return getAt(currentSongIndex );
	}

	public AudioTrack getNextTrack() {
		if (shuffle) {
			currentSongIndex = new Random().nextInt(tracks.size()-1);
		} else if (repeat && atLastTrack()) {
			currentSongIndex = 0;
		} else {
			currentSongIndex++;
		}
		return getAt(currentSongIndex);
	}

	public void setLibrary(MusicLibrary library) {
		this.musicLibrary = library;
	}

	public void generateList(List<Genre> genreList, int trackDuration, int breakDuration) {
		tracks.clear();
		Random random = new Random();
		for (Iterator<Genre> iterator = genreList.iterator(); iterator.hasNext();) {
	        Genre genre = iterator.next();
			List<AudioTrack> tracksForGenre = musicLibrary.getTracksFor(genre);
			if (! tracksForGenre.isEmpty()) {
				int index = random.nextInt(tracksForGenre.size());
				AudioTrack selectedTrack = tracksForGenre.get(index);
				selectedTrack.setTargetDuration(trackDuration);
				this.add(selectedTrack);
				if (iterator.hasNext()) {
					this.add(new BreakTrack(breakDuration));
				}
			}
		}
		currentSongIndex = 0;
	}
	
	public void setTrackDuration(int trackDuration) {
		for (AudioTrack audioTrack : tracks) {
			if (!audioTrack.isBreak()) {
				audioTrack.setTargetDuration(trackDuration);
			}
		}
	}
	
	public void setBreakDuration(int breakDuration) {
		for (AudioTrack track : tracks) {
			if (track.isBreak()) {
				track.setTargetDuration(breakDuration);
			}
		}
	}
	
	public void setTargetTempo(int tempoPercent) {
		for (AudioTrack audioTrack : tracks) {
			if (!audioTrack.isBreak()) {
				audioTrack.setTargetTempoPercent(tempoPercent);
			}
		}
	}

	public int size() {
	    return tracks.size();
    }

}
