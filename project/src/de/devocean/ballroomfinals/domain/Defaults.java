package de.devocean.ballroomfinals.domain;

import java.util.ArrayList;

public class Defaults {

	public static final int FADE_DURATION_MS = 500;
	public static final int SEEK_TIME_MS = 5 * 1000;
	
	private static ArrayList<Genre> orderedGenreList = new ArrayList<Genre>();
	private static int playlistLength = 0;
	private static int trackDuration = 90;
	private static int breakDuration = 60;
	private static int tempoPercent = 100;
	

	static {
		addGenre(new Genre(0, "Samba", 50));
		addGenre(new Genre(1, "Cha Cha", 30));
		addGenre(new Genre(2, "Rumba", 28));
		addGenre(new Genre(3, "Paso Doble", 60));
		addGenre(new Genre(4, "Jive", 43));
	}

	public static int getPlaylistLength() {
		return playlistLength;
	}

	public static void setPlaylistLength(int playlistLength) {
		Defaults.playlistLength = playlistLength;
	}

	public static int getTrackDuration() {
		return trackDuration;
	}

	public static void setTrackDuration(int trackDuration) {
		Defaults.trackDuration = trackDuration;
	}

	public static int getBreakDuration() {
		return breakDuration;
	}

	public static void setBreakDuration(int breakDuration) {
		Defaults.breakDuration = breakDuration;
	}

	public static ArrayList<Genre> getOrderedGenreList() {
		return orderedGenreList;
	}


	public static void addGenre(Genre genre) {
		orderedGenreList.add(genre);
		playlistLength = orderedGenreList.size();
	}

	public static int getTempoPercentage() {
		return tempoPercent;
	}

	public static void setTempoPercentage(int tempoPercent) {
		Defaults.tempoPercent = tempoPercent;
	}

}
