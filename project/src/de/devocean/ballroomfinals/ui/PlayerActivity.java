package de.devocean.ballroomfinals.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;
import de.devocean.ballroomfinals.MainController;
import de.devocean.ballroomfinals.R;
import de.devocean.ballroomfinals.audio.PlayerState;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Defaults;

public class PlayerActivity extends Activity implements
		SeekBar.OnSeekBarChangeListener {

	private ImageButton btnPlay;
	private SeekBar songProgressBar;
	private TextView songTitleLabel;
	private TextView songGenreLabel;
	private TextView songCurrentDurationLabel;
	private TextView songTotalDurationLabel;

	private PlayerState playState;

	private NumberPicker pckTrackDuration;
	private NumberPicker pckBreakDuration;
	private NumberPicker pckTempo;
	private MainController controller;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);

		initUIVariables();
		registerNumberPickerActions();

		controller = new MainController(this);
		playState = controller.getPlayerState();
		songProgressBar.setOnSeekBarChangeListener(this);

		controller.init(getContentResolver());
	}

	private void registerNumberPickerActions() {
		initNumberPicker(pckTrackDuration, 0, 300, 5, Defaults.getTrackDuration());
		pckTrackDuration.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
				 @Override
				 public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					 int trackDuration = Integer.parseInt(picker.getDisplayedValues()[newVal]);
					 controller.updateTrackDuration(trackDuration);
				}
			}
		);
		
		initNumberPicker(pckBreakDuration, 0, 240, 5, Defaults.getBreakDuration());
		pckBreakDuration.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					int breakDuration = Integer.parseInt(picker.getDisplayedValues()[newVal]);
					controller.updateBreakDuration(breakDuration);
				}
			}
		);
		
		initNumberPicker(pckTempo, 85, 115, 1, Defaults.getTempoPercentage());
		pckTempo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					int tempo = Integer.parseInt(picker.getDisplayedValues()[newVal]);
					controller.updateTempo(tempo);
				}
			}
		);
		
	}

	private void initNumberPicker(NumberPicker picker, int minValue, int maxValue, int step, int defaultValue) {
		picker.setMinValue(0);
		String[] values = new String[(maxValue-minValue)/step];
		for (int i=0; i < values.length; i++) {
			values[i] = Integer.toString(minValue + i*step);
		}
		picker.setMaxValue(values.length - 1);
		picker.setValue((defaultValue-minValue) / step);
		picker.setDisplayedValues(values);
		picker.setWrapSelectorWheel(true);
	}

	private void initUIVariables() {
		btnPlay = (ImageButton) findViewById(R.id.btnPlay);
		songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
		songTitleLabel = (TextView) findViewById(R.id.songTitle);
		songGenreLabel = (TextView) findViewById(R.id.songGenre);
		songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
		songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
		pckTrackDuration = (NumberPicker) findViewById(R.id.pckTrackDuration);
		pckBreakDuration = (NumberPicker) findViewById(R.id.pckBreakDuration);
		pckTempo = (NumberPicker) findViewById(R.id.pckTempo);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.player, menu);
		return true;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// Nothing to do; just fulfill the ProgressListener interface
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// Nothing to do; just fulfill the ProgressListener interface
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		controller.seekToPercentage(seekBar.getProgress());
	}

	public void displayTrack(AudioTrack track) {
		songGenreLabel.setText(track.getGenreLabel());
		songTitleLabel.setText(track.getTitle());
		songCurrentDurationLabel.setText(TimeConverter.milliSecondsToTimeString(0));
		songTotalDurationLabel.setText(TimeConverter.secondsToTimeString(track.getTargetDuration()));

		songProgressBar.setProgress(0);
		songProgressBar.setMax(100);
		refreshPlayButton();
    }

	public void refreshPlayButton() {
		if (playState.isPlaying()) {
			btnPlay.setImageResource(R.drawable.btn_pause);
		} else {
			btnPlay.setImageResource(R.drawable.btn_play);
		}
	}

    public void updatePlayPosition(int positionInMillis) {
		songCurrentDurationLabel.setText(TimeConverter.milliSecondsToTimeString(positionInMillis));
		songProgressBar.setProgress(playState.getPositionPercent());
    }

	public void onRegeneratePlaylist(MenuItem item){
		controller.generatePlaylist();
	}

	public void onClickPlay(View view) {
		controller.togglePlay();
	}

	public void onSkipForward(View view) {
		controller.seekForward();
	}

	public void onSkipBackward(View view) {
		controller.seekBackward();
	}

	public void onClickNext(View view) {
		controller.skipForward();
	}

	public void onClickPrevious(View view) {
		controller.skipBackward();
	}
	
}
