package de.devocean.ballroomfinals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Audio.Genres;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Genre;

public class MusicLibrary {

	private String[] id3Fields = { Audio.Media._ID, Audio.Media.TITLE,
			Audio.Media.ARTIST, Audio.Media.ALBUM, Audio.Media.DURATION,
			Audio.Media.DATA };

	private ArrayList<Genre> genres;
	private HashMap<Genre, List<AudioTrack>> allTracks = new HashMap<Genre, List<AudioTrack>>();
	private ContentResolver contentResolver;


	public MusicLibrary(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
		collectGenres();
	}

	public void findMusic() {
		for (Genre genre : genres) {
			Cursor tracks = contentResolver.query(genre.getUri(), id3Fields,
					null, null, null);

			if (tracks.getCount() > 0) {
				List<AudioTrack> genreTracks = allTracks.get(genre);
				if (genreTracks == null) {
					genreTracks = new ArrayList<AudioTrack>();
					allTracks.put(genre, genreTracks);
				}
				for (tracks.moveToFirst(); !tracks.isAfterLast(); tracks
						.moveToNext()) {
					AudioTrack audioTrack = new AudioTrack(tracks.getString(1),
							tracks.getString(2), tracks.getString(3),
							tracks.getInt(4), tracks.getString(5));
					audioTrack.setGenre(genre);
					genreTracks.add(audioTrack);
				}
			}
		}
	}

	private void collectGenres() {
		genres = new ArrayList<Genre>();
		String[] genreFields = { Audio.Genres._ID, Audio.Genres.NAME };
		Cursor cursor = contentResolver.query(Genres.EXTERNAL_CONTENT_URI,
				genreFields, null, null, null);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			genres.add(new Genre(cursor.getInt(0), cursor.getString(1)));
		}
	}

	public List<AudioTrack> getAllTracks() {
		ArrayList<AudioTrack> all = new ArrayList<AudioTrack>();
		for (List<AudioTrack> currentGenre : allTracks.values()) {
			all.addAll(currentGenre);
		}
		return all;
	}

	public List<AudioTrack> getTracksFor(Genre genre) {
		if (! allTracks.containsKey(genre)) {
			return new ArrayList<AudioTrack>();
		}
		return allTracks.get(genre);
	}

}
