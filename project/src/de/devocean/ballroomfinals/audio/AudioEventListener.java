package de.devocean.ballroomfinals.audio;


public interface AudioEventListener {

	public void updatePlayPosition(int positionInMillis);

	public void stoppedPlaying();

}
