package de.devocean.ballroomfinals.audio;

public interface PlayerState {

	public abstract int getCurrentPosition();

	public abstract int getPositionPercent();

	public abstract boolean isPlaying();

	public abstract void registerCompletionListener(AudioEventListener controller);

}
