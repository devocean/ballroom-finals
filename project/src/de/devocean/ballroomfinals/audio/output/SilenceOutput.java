package de.devocean.ballroomfinals.audio.output;

import android.os.Handler;
import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.domain.AudioTrack;


public class SilenceOutput implements AudioOutput {

	private static final int TICKS_MILLIS = 50;

	private final TrackPlayer trackPlayer;
	private Handler timerHandler = new Handler();
	private int targetDuration = 0;
	private int currentPosition = 0;
	private boolean currentlyPlaying = false;
	private long lastUpdateTimeMillis;

	public SilenceOutput(TrackPlayer trackPlayer) {
		this.trackPlayer = trackPlayer;
    }

	@Override
    public void initialize() {
    }

	@Override
    public void release() {
		timerHandler.removeCallbacks(positionCallback);
    }

	@Override
    public void play(AudioTrack track) throws AudioException {
		this.targetDuration = track.getTargetDuration() * 1000;
		resume();
    }

	@Override
    public void pause() {
		stop();
    }

	@Override
    public void resume() {
		currentlyPlaying = true;
		lastUpdateTimeMillis = System.currentTimeMillis();
		timerHandler.postDelayed(positionCallback, TICKS_MILLIS);
    }

	@Override
    public void stop() {
		currentlyPlaying = false;
		timerHandler.removeCallbacks(positionCallback);
    }

	@Override
    public void seekTo(int positionMillis) {
		currentPosition = Math.min(positionMillis, targetDuration);
    }

	@Override
    public int getCurrentPositionMillis() {
	    return currentPosition;
    }

	@Override
    public void fadeOut(TrackPlayer trackPlayer) {
		stop();
		trackPlayer.trackHasEnded();
    }

	@Override
    public boolean isPlaying() {
	    return currentlyPlaying;
    }

	private Runnable positionCallback = new Runnable() {
		public void run() {
			long now = System.currentTimeMillis();
			currentPosition += now - lastUpdateTimeMillis;
			lastUpdateTimeMillis = now;
			if (currentPosition < targetDuration) {
				timerHandler.postDelayed(this, TICKS_MILLIS);
			} else {
				fadeOut(trackPlayer);
			}
		};
	};
}
