package de.devocean.ballroomfinals.audio.output;

import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.domain.AudioTrack;


public interface AudioOutput {

	public void initialize();

	public void release();

	public void play(AudioTrack track) throws AudioException;

	public void pause();

	public void resume();

	public void stop();

	public void seekTo(int positionMillis);

	public int getCurrentPositionMillis();

	public void fadeOut(TrackPlayer trackPlayer);

	public boolean isPlaying();

}
