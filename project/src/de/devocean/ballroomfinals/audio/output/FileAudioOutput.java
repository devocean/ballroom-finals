package de.devocean.ballroomfinals.audio.output;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import de.devocean.ballroomfinals.audio.FileAudioFader;
import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Defaults;


public class FileAudioOutput implements AudioOutput, OnCompletionListener {

	MediaPlayer mediaPlayer;
	private final TrackPlayer trackPlayer;

	public FileAudioOutput(TrackPlayer player) {
		this.trackPlayer = player;
	}

	@Override
    public void initialize() {
    }

	@Override
    public void release() {
		if (mediaPlayer != null) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
    }

	@Override
    public void play(AudioTrack track) throws AudioException {
		mediaPlayer = new MediaPlayer();
		mediaPlayer.reset();
		try {
	        mediaPlayer.setDataSource(track.getFilePath());
	        mediaPlayer.prepare();
        } catch (Exception e) {
	        throw new AudioException(e.getLocalizedMessage());
        }
		mediaPlayer.start();
		mediaPlayer.setOnCompletionListener(this);
    }

	@Override
    public void pause() {
		if (mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
		}
    }

	@Override
    public void resume() {
		// TODO refactor for re-instantiation of MediaPlayer on resume
    	if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
    		mediaPlayer.start();
    	}
    }

	@Override
    public void stop() {
		if (mediaPlayer == null) return;
    	release();
    }

	@Override
    public void seekTo(int positionMillis) {
		mediaPlayer.seekTo(positionMillis);
    }

	@Override
    public int getCurrentPositionMillis() {
	    if (mediaPlayer == null) return 0;
	    return mediaPlayer.getCurrentPosition();
    }

	@Override
    public void onCompletion(MediaPlayer mp) {
	    trackPlayer.trackHasEnded();
    }

	@Override
    public void fadeOut(TrackPlayer trackPlayer) {
		new FileAudioFader(mediaPlayer, trackPlayer, Defaults.FADE_DURATION_MS).execute();
		mediaPlayer = null;
    }

	@Override
    public boolean isPlaying() {
		return mediaPlayer != null && mediaPlayer.isPlaying();
    }

}
