package de.devocean.ballroomfinals.audio.output;

import java.io.IOException;

import com.smp.soundtouchandroid.OnProgressChangedListener;
import com.smp.soundtouchandroid.SoundStreamAudioPlayer;

import de.devocean.ballroomfinals.audio.TimestretchAudioFader;
import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Defaults;

public class TimestretchAudioOutput implements AudioOutput, OnProgressChangedListener {

	
	private TrackPlayer trackPlayer;
	private SoundStreamAudioPlayer player;
	private TimestretchAudioFader fader;

	public TimestretchAudioOutput(TrackPlayer trackPlayer) {
		this.trackPlayer = trackPlayer;
	}
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
	}

	@Override
	public void release() {
		stop();
		if (fader != null) {
			fader.cancel(true);
			fader = null;
		}
		player = null;
		
	}

	@Override
	public void play(AudioTrack track) throws AudioException {
		try {
			player = new SoundStreamAudioPlayer(0, track.getFilePath(), track.getTargetTempoPercent() / 100.0f, 0.0f);
			new Thread(player).start();
			player.start();
			player.setOnProgressChangedListener(this);
		} catch (IOException e) {
			throw new AudioException(e.getLocalizedMessage());
		}
	}

	@Override
	public void pause() {
		if (player == null) {
			return;
		}
		player.pause();
	}

	@Override
	public void resume() {
		if (player == null) {
			return;
		}
		if (player.isPaused()) {
			player.start();
		}
	}

	@Override
	public void stop() {
		if (player == null) {
			return;
		}
		if (player.isInitialized()) {
			player.stop();
		}
	}

	@Override
	public void seekTo(int positionMillis) {
		if (player == null) {
			return;
		}
		player.seekTo(positionMillis * 1000);
	}

	@Override
	public int getCurrentPositionMillis() {
		if (player == null) {
			return 0;
		}
		return (int) (player.getPlayedDuration() / 1000);
	}

	@Override
	public void fadeOut(TrackPlayer trackPlayer) {
		if (player == null) return;
		fader = new TimestretchAudioFader(player, trackPlayer, Defaults.FADE_DURATION_MS);
		fader.execute();
	}

	@Override
	public boolean isPlaying() {
		if (player == null) {
			return false;
		}
		return !(player.isPaused() || player.isFinished());
	}

	@Override
	public void onProgressChanged(int track, double currentPercentage,
			long position) {
		
	}

	@Override
	public void onTrackEnd(int track) {
		trackPlayer.trackHasEnded();
	}

	@Override
	public void onExceptionThrown(String string) {
		// TODO Auto-generated method stub
		
	}

}
