package de.devocean.ballroomfinals.audio.output;


public class AudioException extends Exception {

    private static final long serialVersionUID = 3023456810753829395L;


    public AudioException(String message) {
    	super(message);
    }
}
