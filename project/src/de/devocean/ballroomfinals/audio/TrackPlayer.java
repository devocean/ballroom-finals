package de.devocean.ballroomfinals.audio;

import android.os.Handler;
import android.widget.SeekBar;
import de.devocean.ballroomfinals.audio.output.AudioException;
import de.devocean.ballroomfinals.audio.output.AudioOutput;
import de.devocean.ballroomfinals.audio.output.FileAudioOutput;
import de.devocean.ballroomfinals.audio.output.SilenceOutput;
import de.devocean.ballroomfinals.audio.output.TimestretchAudioOutput;
import de.devocean.ballroomfinals.domain.AudioTrack;

public class TrackPlayer implements PlayerState {

	private AudioOutput output;
	private AudioTrack currentTrack;
	private Handler timerHandler = new Handler();
	private AudioEventListener listener;

	public TrackPlayer() {
		// keep the default constructor
	}

    public void play(AudioTrack track) throws AudioException {
    	if (output != null) {
    		clear();
    	}
		if (track == AudioTrack.NULL_TRACK)  {
			return;
		}
    	this.currentTrack = track;
    	output = createOutputFor(currentTrack);
    	output.play(currentTrack);

    	timerHandler.postDelayed(checkPlayDuration, 1000);
    	timerHandler.postDelayed(updatePlayPosition, 100);

    }
    
    public void updateTempo() throws AudioException {
    	int lastPlayPosition = getCurrentPosition();
    	boolean isPaused = !isPlaying();
    	output.stop();
    	output.release();
    	play(currentTrack);
    	if (isPaused) {
    		pause();
    	}
    	output.seekTo(lastPlayPosition);
    }

    private AudioOutput createOutputFor(AudioTrack track) {
    	if (track.isBreak()) {
    		return new SilenceOutput(this);
    	} else {
    		return new TimestretchAudioOutput(this);
    	}
    }

    @Override
	public boolean isPlaying() {
		return output.isPlaying();
	}

    public void pause() {
		output.pause();
	}

    public void resume() {
		output.resume();
    }

    public void stop() {
		output.stop();
    	trackHasEnded();
    }

    public void skipForward(int timeInMillis) {
		int currentPosition = getCurrentPosition();
		if (currentPosition + timeInMillis <= currentTrack.getDuration()) {
			output.seekTo(currentPosition + timeInMillis);
		} else {
			output.seekTo(currentTrack.getDuration());
		}
	}

    public void skipBackward(int timeInMillis) {
		int currentPosition = getCurrentPosition();
		if (currentPosition - timeInMillis >= 0) {
			output.seekTo(currentPosition - timeInMillis);
		} else {
			output.seekTo(0);
		}
	}

    public void skipToPercentage(int positionPercent) {
    	int targetPosition = percentageToMillis(positionPercent, getTargetPlayDuration());
    	output.seekTo(targetPosition);
    }

    @Override
	public int getPositionPercent() {
		long currentDuration = getCurrentPosition();
		int progress = (int) (getProgressPercentage(currentDuration, getTargetPlayDuration()));
		return progress;
	}


    @Override
	public int getCurrentPosition() {
		if (output == null) return 0;
		return output.getCurrentPositionMillis();
    }

	public long getTargetPlayDuration() {
		return currentTrack.getTargetDuration() * 1000;
	}

	// TODO: Refactor badly done integer casting
	private int getProgressPercentage(long currentDuration, long totalDuration){
		Double percentage = (double) 0;

		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);

		percentage =(((double)currentSeconds)/totalSeconds)*100;
		return percentage.intValue();
	}



    @Override
	public void registerCompletionListener(AudioEventListener controller) {
		this.listener = controller;
	}

	private int percentageToMillis(int progress, long targetDuration) {
		int currentDuration = 0;
		targetDuration = (int) (targetDuration / 1000);
		currentDuration = (int) ((((double)progress) / 100) * targetDuration);

		return currentDuration * 1000;
	}


	public void trackHasEnded() {
		clear();
		listener.stoppedPlaying();
	}

	private void clear() {
		timerHandler.removeCallbacks(updatePlayPosition);
		timerHandler.removeCallbacks(checkPlayDuration);
		output.release();
	}


	private Runnable checkPlayDuration = new Runnable() {

	    public void run(){
	    	if (getCurrentPosition() > getTargetPlayDuration()) {
	    		output.fadeOut(TrackPlayer.this);
	    		return;
	    	} else {
	    		timerHandler.postDelayed(checkPlayDuration, 1000);
	    	}
	    }
	};


	private Runnable updatePlayPosition = new Runnable() {
		public void run() {
			if (listener == null) return;
			listener.updatePlayPosition(getCurrentPosition());
			timerHandler.postDelayed(this, 100);
		}
	};


}
