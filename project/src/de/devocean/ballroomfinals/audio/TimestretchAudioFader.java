package de.devocean.ballroomfinals.audio;

import com.smp.soundtouchandroid.SoundStreamAudioPlayer;

import android.media.MediaPlayer;
import android.os.AsyncTask;

public class TimestretchAudioFader extends AsyncTask<String, Integer, String> {

	private SoundStreamAudioPlayer mediaPlayer;
	private TrackPlayer trackPlayer;
	private int fadeDurationMillis;


	public TimestretchAudioFader(SoundStreamAudioPlayer player, TrackPlayer trackPlayer, int durationInMillis) {
		this.mediaPlayer = player;
		this.trackPlayer = trackPlayer;
		this.fadeDurationMillis = durationInMillis;
	}


	@Override
	protected String doInBackground(String... args) {
		float level = 0.8f;
		int stepDurationMillis = 10;

		for (int i = 0; i < fadeDurationMillis; i += stepDurationMillis) {
			if (isCancelled()) {
				break;
			}
			if (mediaPlayer != null) {
				level = level * 0.9f;
				mediaPlayer.setVolume(level, level);
			}
			try {
				Thread.sleep(stepDurationMillis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return "dummy";
	}


	@Override
	protected void onPostExecute(String dummy) {
		if (mediaPlayer != null) {
			if (mediaPlayer.isInitialized()) {
				mediaPlayer.setVolume(0, 0);
				mediaPlayer.stop();
			}
			trackPlayer.trackHasEnded();
			mediaPlayer = null;
		}
	}


	@Override
	public void onCancelled() {
		if (mediaPlayer != null) {
			mediaPlayer.setVolume(0, 0);
			mediaPlayer = null;
		}
		trackPlayer = null;
	}
}
