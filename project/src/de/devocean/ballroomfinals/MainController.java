package de.devocean.ballroomfinals;

import android.content.ContentResolver;
import de.devocean.ballroomfinals.audio.AudioEventListener;
import de.devocean.ballroomfinals.audio.PlayerState;
import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.audio.output.AudioException;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Defaults;
import de.devocean.ballroomfinals.domain.Playlist;
import de.devocean.ballroomfinals.ui.PlayerActivity;

public class MainController implements AudioEventListener {
	private PlayerActivity ui;
	
	private Playlist playlist;
	private TrackPlayer player;

	public MainController(PlayerActivity playerActivity) {
		this.ui = playerActivity;
		player = new TrackPlayer();
		player.registerCompletionListener(this);
	}
	
	public PlayerState getPlayerState() {
		return player;
	}

	public void init(ContentResolver contentResolver) {
		playlist = new Playlist();
		MusicLibrary library = new MusicLibrary(contentResolver);
		library.findMusic();
		playlist.setLibrary(library);
		generatePlaylist();
	}
	
	public void generatePlaylist() {
	    playlist.generateList(Defaults.getOrderedGenreList(), Defaults.getTrackDuration(), Defaults.getBreakDuration());
	    playTrack(playlist.first());
	    player.pause();
	    ui.refreshPlayButton();
    }
	
	public void playTrack(AudioTrack track) {
		try {
	        player.play(track);
        } catch (AudioException e) {
	        // TODO display Error-Dialog to user
	        e.printStackTrace();
        }
		ui.displayTrack(track);
	}
	
	public void togglePlay() {
		if (player.isPlaying()) {
			player.pause();
		} else {
			player.resume();
		}
		ui.refreshPlayButton();
	}

	public void skipForward() {
		AudioTrack nextTrack = playlist.getNextTrack();
		playTrack(nextTrack);
	}

	public void skipBackward() {
		AudioTrack previousTrack = playlist.getPreviousTrack();
		playTrack(previousTrack);
	}

	public void seekToPercentage(int positionPercent) {
		player.skipToPercentage(positionPercent);
	}

	public void seekForward() {
		player.skipForward(Defaults.SEEK_TIME_MS);
	}

	public void seekBackward() {
		player.skipBackward(Defaults.SEEK_TIME_MS);
	}
	
	public void updateTrackDuration(int trackDuration) {
		playlist.setTrackDuration(trackDuration);
		updatePlayerUI();
	}

	private void updatePlayerUI() {
		ui.displayTrack(playlist.getCurrentTrack());
		ui.updatePlayPosition(player.getCurrentPosition());
	}

	public void updateBreakDuration(int breakDuration) {
		playlist.setBreakDuration(breakDuration);
		updatePlayerUI();
	}

	public void updateTempo(int tempo) {
		playlist.setTargetTempo(tempo);
		try {
			player.updateTempo();
		} catch (AudioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void updatePlayPosition(int positionInMillis) {
		ui.updatePlayPosition(positionInMillis);
	}

	@Override
	public void stoppedPlaying() {
		AudioTrack nextTrack = playlist.getNextTrack();
		if (nextTrack != AudioTrack.NULL_TRACK) {
			playTrack(nextTrack);
		} else {
			ui.displayTrack(AudioTrack.NULL_TRACK);
		}
	}
}
