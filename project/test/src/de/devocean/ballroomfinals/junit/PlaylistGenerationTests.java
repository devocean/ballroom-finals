package de.devocean.ballroomfinals.junit;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.devocean.ballroomfinals.MusicLibrary;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.BreakTrack;
import de.devocean.ballroomfinals.domain.Defaults;
import de.devocean.ballroomfinals.domain.Genre;
import de.devocean.ballroomfinals.domain.Playlist;

public class PlaylistGenerationTests extends TestCase {

	@Mock
	MusicLibrary library;

	private Playlist playlist;

	private static List<AudioTrack> sambaTracks = new ArrayList<AudioTrack>();
	private static List<AudioTrack> chachaTracks = new ArrayList<AudioTrack>();
	private static List<AudioTrack> rumbaTracks = new ArrayList<AudioTrack>();
	private static List<AudioTrack> jiveTracks = new ArrayList<AudioTrack>();

	static {
		chachaTracks.add(new AudioTrack("Pao Pao", "SomeArtist", "Latin Hits", 184, "paopao.mp3"));
		chachaTracks.add(new AudioTrack("Mean Spirited Sal", "SomeArtist", "Latin Hits", 143, "meansal.mp3"));
		sambaTracks.add(new AudioTrack("Africa", "SomeArtist", "Latin Hits", 206, "africa.mp3"));
		rumbaTracks.add(new AudioTrack("Besame Mucho", "SomeArtist", "Latin Hits", 244, "besame.mp3"));
		rumbaTracks.add(new AudioTrack("Penelope", "SomeArtist", "Latin Hits", 231, "penelope.mp3"));
		rumbaTracks.add(new AudioTrack("Come as you are", "SomeArtist", "Latin Hits", 211, "come.mp3"));
		rumbaTracks.add(new AudioTrack("Ni tu ni yo", "SomeArtist", "Latin Hits", 264, "tuyo.mp3"));
		rumbaTracks.add(new AudioTrack("Golden Dawn", "SomeArtist", "Latin Hits", 191, "dawn.mp3"));
		rumbaTracks.add(new AudioTrack("E isso ai", "SomeArtist", "Latin Hits", 221, "isso.mp3"));
		jiveTracks.add(new AudioTrack("Shake", "SomeArtist", "Latin Hits", 120, "shake.mp3"));
		jiveTracks.add(new AudioTrack("Reet Petite", "SomeArtist", "Latin Hits", 84, "reetpetite.mp3"));
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		playlist = new Playlist();
		MockitoAnnotations.initMocks(this);
	}

	public void testAutocreateTrackList() {
		when(library.getTracksFor(argThat(new IsGenre("Samba")))).thenReturn(sambaTracks);
		when(library.getTracksFor(argThat(new IsGenre("Cha Cha")))).thenReturn(chachaTracks);
		when(library.getTracksFor(argThat(new IsGenre("Rumba")))).thenReturn(rumbaTracks);
		when(library.getTracksFor(argThat(new IsGenre("Jive")))).thenReturn(jiveTracks);

		playlist.setLibrary(library);
		int playDuration = 45;
		int pauseDuration = 10;
		playlist.generateList(Defaults.getOrderedGenreList(), playDuration, pauseDuration);

		assertEquals(7, playlist.size());
		assertThat(sambaTracks, hasItem(playlist.getAt(0)));
		assertThat(playlist.getAt(1), instanceOf(BreakTrack.class));
		assertThat(chachaTracks, hasItem(playlist.getAt(2)));
		assertThat(playlist.getAt(3), instanceOf(BreakTrack.class));
		assertThat(rumbaTracks, hasItem(playlist.getAt(4)));
		assertThat(playlist.getAt(5), instanceOf(BreakTrack.class));
		assertThat(jiveTracks, hasItem(playlist.getAt(6)));
	}

	class IsGenre extends ArgumentMatcher<Genre> {

		private String genreName;

		IsGenre(String genreName) {
			this.genreName = genreName;
		}

		@Override
		public boolean matches(Object genre) {
			if (genre == null) return false;
			return ((Genre) genre).getName().equalsIgnoreCase(genreName);
		}

	}

}
