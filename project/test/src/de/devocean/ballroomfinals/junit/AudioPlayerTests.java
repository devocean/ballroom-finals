package de.devocean.ballroomfinals.junit;

import static org.hamcrest.MatcherAssert.*;
import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import android.media.MediaPlayer;
import de.devocean.ballroomfinals.audio.TrackPlayer;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Defaults;


public class AudioPlayerTests extends TestCase {

	@Mock
	MediaPlayer mockMediaPlayer;

	@Override
	protected void setUp() throws Exception {
	    super.setUp();
	    MockitoAnnotations.initMocks(this);
	}

	public void testTimedStop() throws Exception {
		Defaults.setTrackDuration(30);
		AudioTrack track = new AudioTrack("Pao Pao", "SomeArtist", "Latin Hits", 184, "paopao.mp3");
		TrackPlayer player = new TrackPlayer();
		player.play(track);
		player.skipForward(29500);
		assertThat("Player is still playing", player.isPlaying());
		player.skipForward(1000);
		assertThat("Player is stopped", !player.isPlaying());
	}

}
