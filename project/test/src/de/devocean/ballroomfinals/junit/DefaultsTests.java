package de.devocean.ballroomfinals.junit;

import junit.framework.TestCase;
import de.devocean.ballroomfinals.domain.Defaults;
import de.devocean.ballroomfinals.domain.Genre;

public class DefaultsTests extends TestCase {

	public void testDefaultValues() {
		assertEquals(5, Defaults.getPlaylistLength());
		assertEquals(new Genre(1, "Cha Cha"), Defaults.getOrderedGenreList().get(1));
		assertEquals(new Genre(4, "Jive"), Defaults.getOrderedGenreList().get(4));

		assertEquals(90, Defaults.getTrackDuration());
		assertEquals(60, Defaults.getBreakDuration());
		assertEquals(100, Defaults.getTempoPercentage());
	}

}
