package de.devocean.ballroomfinals.junit;

import junit.framework.TestCase;
import de.devocean.ballroomfinals.domain.AudioTrack;
import de.devocean.ballroomfinals.domain.Playlist;

public class PlaylistTests extends TestCase {
	
	private static final AudioTrack NULL_TRACK = AudioTrack.NULL_TRACK;
	private Playlist playlist;
	private AudioTrack trackOne;
	private AudioTrack trackTwo;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		playlist = new Playlist();
	}
	
	public void testEmptyList() {
		assertEquals(NULL_TRACK, playlist.first());
		assertEquals(NULL_TRACK, playlist.last());
		assertEquals(NULL_TRACK, playlist.getNextTrack());
		assertEquals(NULL_TRACK, playlist.getPreviousTrack());
		assertEquals(NULL_TRACK, playlist.getAt(0));
	}
	
	public void testOneTrack() {
		playlist.add(trackOne);
		assertEquals(trackOne, playlist.first());
		assertEquals(trackOne, playlist.last());
		assertEquals(trackOne, playlist.getAt(0));
		assertEquals(NULL_TRACK, playlist.getNextTrack());
		assertEquals(trackOne, playlist.getPreviousTrack());
		assertEquals(NULL_TRACK, playlist.getPreviousTrack());
	}
	
	public void testTwoTracks() {
		//add two tracks, verify their order, then skip beyond the limits
		playlist.add(trackOne);
		playlist.add(trackTwo);
		assertEquals(trackOne, playlist.first());
		assertEquals(trackTwo, playlist.last());
		assertEquals(trackOne, playlist.getAt(0));
		assertEquals(trackTwo, playlist.getAt(1));
		
		assertEquals(NULL_TRACK, playlist.getNextTrack());
		
		assertEquals(trackTwo, playlist.getPreviousTrack());
		assertEquals(trackOne, playlist.getPreviousTrack());
		assertEquals(NULL_TRACK, playlist.getPreviousTrack());
		
		assertEquals(trackOne, playlist.getNextTrack());
	}
	
	public void testRepeatOneTrack() {
		playlist.add(trackOne);
		playlist.add(trackTwo);
		boolean repeatState = playlist.toggleRepeat();
		assertEquals(true, repeatState);
		playlist.last();
		assertEquals(trackOne, playlist.getNextTrack());
	}

	public void testRepeatTwoTracks() {
		playlist.add(trackOne);
		boolean repeatState = playlist.toggleRepeat();
		assertEquals(true, repeatState);
		playlist.last();
		assertEquals(trackOne, playlist.getNextTrack());
	}
	
	public void testShuffle() {
		//TODO: how can we test the randomized shuffle?
	}

}
